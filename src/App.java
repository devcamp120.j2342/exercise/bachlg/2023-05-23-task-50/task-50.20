import java.util.Arrays;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        String array1 = "Devcamp";
        Object[] array2 = {1, 2, 3};
        boolean isArrayResult1 = isArray(array1);
        boolean isArrayResult2 = isArray(array2);
        System.out.println("Is array: " + isArrayResult1);
        System.out.println("Is array: " + isArrayResult2);

        Object[] array3 = {1, 2, 3, 4, 5, 6};
        int n1 = 3;
        int n2 = 6;
        Object element3 = getElement(array3, n1);
        Object element6 = getElement(array3, n2);
        System.out.println("Element at index " + n1 + ": " + element3);
        System.out.println("Element at index " + n2 + ": " + element6);

        int[] array4 = {3, 8, 7, 6, 5, -4, -3, 2, 1};
        sortArray(array4);
        System.out.println("Sorted array: " + Arrays.toString(array4));

        int[] array5 = {1, 2, 3, 4, 5, 6};
        int n3 = 3;
        int n4 = 7;
        int index1 = findIndex(array5, n3);
        int index2 = findIndex(array5, n4);
        System.out.println("Index of element " + n3 + ": " + index1);
        System.out.println("Index of element " + n4 + ": " + index2);

        Object[] array6 = {1, 2, 3};
        Object[] array7 = {4, 5, 6};
        Object[] mergedArray = mergeArrays(array6, array7);
        System.out.println("Merged array: " + Arrays.toString(mergedArray));

    }
    public static boolean isArray(Object variable) {
        return variable instanceof Object[];
    }

    public static Object getElement(Object[] array, int n) {
        if (array == null || n < 0 || n >= array.length) {
            return null;
        }
        return array[n];
    }
    
    public static void sortArray(int[] array) {
        Arrays.sort(array);
    }
    
    public static int findIndex(int[] array, int n) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == n) {
                return i;
            }
        }
        return -1;
    }
    
    public static Object[] mergeArrays(Object[] array1, Object[] array2) {
        if (array1 == null && array2 == null) {
            return null;
        }
        if (array1 == null) {
            return array2;
        }
        if (array2 == null) {
            return array1;
        }
        Object[] mergedArray = new Object[array1.length + array2.length];
        System.arraycopy(array1, 0, mergedArray, 0, array1.length);
        System.arraycopy(array2, 0, mergedArray, array1.length, array2.length);
        return mergedArray;
    }
    
}
